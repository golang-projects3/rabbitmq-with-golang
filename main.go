package main

import (
	// "context"
	"context"
	"fmt"
	"log"

	amqp "github.com/rabbitmq/amqp091-go"
)



func Consume(ch *amqp.Channel) {
	msgs, err := ch.Consume(
		"minha_fila",
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		panic(err)
	}

	for msg := range msgs {
		fmt.Println(string(msg.Body))
	}
}

func Produce(ch *amqp.Channel, msg string) {


	err := ch.PublishWithContext(
		context.Background(),
		"",
		"minha_fila",
		true,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
      Body:        []byte(msg),
		},
	)
	if err != nil {
		log.Fatalf("Não foi possível publicar a mensagem: %v", err)
		return
  }
}

func main() {

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")

	if err != nil {
		panic(err)
	}

	defer conn.Close()

	ch, err := conn.Channel()

	if err != nil {
		panic(err)
	}


	Consume(ch)


}